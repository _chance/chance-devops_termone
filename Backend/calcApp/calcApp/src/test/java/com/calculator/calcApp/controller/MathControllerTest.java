package com.calculator.calcApp.controller;

import com.calculator.calcApp.dto.DoMathRequest;
import com.calculator.calcApp.utils.ResponseBody;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class MathControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    private String apiPath = "/api/v1/calculator";

    @Test
    public void endpointOfCalculatingShouldReturnTheResult(){
        DoMathRequest body = new DoMathRequest(12, 34, "+");
        ResponseEntity<ResponseBody> response = this.restTemplate.postForEntity(apiPath+"/calculate",body, ResponseBody.class);

        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertEquals(46,response.getBody().getResult());
    }

}