package com.calculator.calcApp.service;

import com.calculator.calcApp.exceptions.InvalidOperationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MathOperatorImplTest {

    MathOperatorImpl mathOperator = new MathOperatorImpl();

    @Test
    public void addition_success() throws InvalidOperationException {
        assertEquals(12, mathOperator.doMath(6,6, "+"));
    }
    @Test
    public void multiplication_success() throws InvalidOperationException {
        assertEquals(16, mathOperator.doMath(2, 8, "*"));
    }

    @Test
    public void division_success() throws InvalidOperationException {
        assertEquals(2, mathOperator.doMath(16, 8, "/"));
    }

    @Test
    public void exponents_success() throws InvalidOperationException {
        assertEquals(8, mathOperator.doMath(2, 3, "**"));
    }
    @Test
    public void logarithm_success() throws InvalidOperationException {
        assertEquals(8, mathOperator.doMath(16, 8, "-"));
    }
    @Test
    public void ln() throws InvalidOperationException {
        assertEquals(8, mathOperator.doMath(16, 8, "-"));
    }

}