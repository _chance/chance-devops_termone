package com.calculator.calcApp.controller;

import com.calculator.calcApp.dto.DoMathRequest;
import com.calculator.calcApp.exceptions.InvalidOperationException;
import com.calculator.calcApp.service.MathOperatorImpl;
import com.calculator.calcApp.utils.ResponseBody;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/calculator")
@NoArgsConstructor
@Data
public class MathController {

    private MathOperatorImpl mathOperator = new MathOperatorImpl();

    @PostMapping("/calculate")
    public ResponseBody doMath(@RequestBody DoMathRequest doMathRequest) throws InvalidOperationException {
       double result = mathOperator.doMath(doMathRequest.getOperand1(), doMathRequest.getOperand2(), doMathRequest.getOperation());
        return new ResponseBody("Calculations complete", result);
    }


}
