package com.calculator.calcApp.service;

import com.calculator.calcApp.exceptions.InvalidOperationException;

public interface MathOperator {
    public double doMath(double operand1, double operand2, String operation) throws InvalidOperationException;
}
