package com.calculator.calcApp.exceptions;

public class InvalidOperationException extends Exception{
    public InvalidOperationException(String message){
        super(message);
    }
}
