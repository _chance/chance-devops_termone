describe('Calculator.cy.js', () => {
  it('Tests POST METHOD', () => {
    cy.request({
      method: "POST",
      url: "http://localhost:8085/api/v1/calculator/calculate",
      body: {
        "operand1" : 2,
        "operand2": 3,
        "operation": "-"
      },
      headers: {
        "content-type": "application/json"
      }, 
    }).then((res) =>{
      expect(res.body).to.deep.equal({
        "message": "Calculations complete",
        "result": -1.0
      });
    })
  })
})