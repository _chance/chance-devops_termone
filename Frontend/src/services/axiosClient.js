import axios from 'axios'

const baseUrl = 'http://127.0.0.1:5000/api/v1/'

const axiosClient = axios.create({
  baseURL: baseUrl
})

axiosClient.interceptors.request.use(async config => {
  return {
    ...config,
    headers: {
      'Content-Type': 'application/json',
      'mode': 'no-cors'
    }
  }
})

axiosClient.interceptors.response.use(response => {
  if (response && response.data) return response.data
  return response
}, err => {
  if (!err.response) {
    return alert(err)
  }
  throw err.response
})

export default axiosClient