import './App.css';
import { Switch, Route } from "react-router-dom"
import { Calculator } from './components/calculator';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route strict exact path="/" component={Calculator}/>
      </Switch>
    </div>
  );
}

export default App;
