import axios from 'axios';
import React, { useState } from 'react';
import BASE_URL from '../constants/baseUrl'
// import axiosClient from '../services/axiosClient';
export const Calculator = () => {
  const [operand1, setOperand1] = useState(0);
  const [operand2, setOperand2] = useState(0);
  const [operator, setOperator] = useState('');
  const [result, setResult] = useState(0);

  const handleOperand1Change = (event) => {
    setOperand1(parseInt(event.target.value, 10));
  };

  const handleOperand2Change = (event) => {
    setOperand2(parseInt(event.target.value, 10));
  };

  const handleOperatorChange = (event) => {
    setOperator(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log("Hello")
    // event.preventDefault();
    // if (operator === '+') {
    //   setResult(operand1 + operand2);
    // } else if (operator === '-') {
    //   setResult(operand1 - operand2);
    // } else if (operator === '*') {
    //   setResult(operand1 * operand2);
    // } else if (operator === '/') {
    //   setResult(operand1 / operand2);
    // }
    axios.post(`http://localhost:8085/api/v1/calculator`, {
      operand1, operand2, operator
    })
    .then(function (response) {
      console.log(response);
      setResult(response.data.result);
    })
    .catch(function (error) {
      console.log(error);
    })
  };

  return (
    <div className='my-5 text-center'>
      <h1 className='text-xl '>Welcome to Our React Calculator</h1>
  <form onSubmit={handleSubmit} className="">
     <div className='my-3'>
     <label>
        Operand 1:
        <input type="number" value={operand1} onChange={handleOperand1Change} placeholder="Enter first number" />
      </label>
     </div>
      <div className='my-3'>
      <label>
        Operator:
        <select value={operator} onChange={handleOperatorChange} name="operator" className="h-full rounded-md border-transparent bg-transparent py-0 pl-2 pr-7 text-gray-500 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
        <option value="">Select</option>
          <option value="+">+</option>
          <option value="-">-</option>
          <option value="*">*</option>
          <option value="/">/</option>
          <option value="**">**</option>
          <option value="log">log</option>
          <option value="ln">ln</option>
      </select>
      </label>
      </div>
      <div className='my-3'>
      <label>
        Operand 2:
        </label>
        <input type="number" value={operand2} onChange={handleOperand2Change}  placeholder="Enter second number"/>
      </div>
    <div>
      <input type='submit' className='btn btn-primary w-10 h-4 text-white' data-testid="Submit"/>
    </div>
    </form>
      { result  && "The Output of your operation is:"
        (
          <div>
           { result}
          </div>
        )
      }
    </div>
    
    )}