import { render, screen } from "@testing-library/react"
import { Calculator } from "../calculator"
import '@testing-library/jest-dom'
import React from 'react';

describe("Calculator", () =>{
    describe("Testing the UI components", () =>{
        it("Should render the operand 1 label on screen", () =>{
            render(<Calculator/>)
            const operand1Label = screen.getByText(/Operand 1:/i)
            // expect(operand1Label).toBeInTheDocument();
            expect(operand1Label).toBeInTheDocument();
        })

        it("Should render the operand2 label on screen", () =>{
            render(<Calculator/>)
            const operand2Label = screen.getByText(/Operand 2:/i)
            expect(operand2Label).toBeInTheDocument();
        })

        it("Should render the operand 1 input on screen", () =>{
            render(<Calculator/>)
            const operand1Input = screen.getByPlaceholderText(/Enter first number/i)
            expect(operand1Input).toBeInTheDocument();
        })

        it("Should render the operand 2 input on screen", () =>{
            render(<Calculator/>)
            const operand2Input = screen.getByPlaceholderText(/Enter second number/i)
            expect(operand2Input).toBeInTheDocument();
        })

        it("Should render button input on screen", () =>{
            render(<Calculator/>)
            const button = screen.getByTestId(/Submit/i)
            expect(button).toBeInTheDocument();
        })
        
    })
})