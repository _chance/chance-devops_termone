# Chance-DEVOPS_TermOne



## Getting started

The Main page shows three inputs: One for the two operands, and another to select the operator.
Once submitted, one is able to see the results from the backend. 

## Frontend

The frontend is a simple page tested using Jest. When the page reloads, one should see the text "Welcome to Our React Calculator"

Once the button submit is clicked, the data is cleared, and after a few seconds, the user is able to see the result of the operation as generated from the backend.


## Backend

The backend accepts data from the frontend. It has a simple end point called from the frontend whose method performs the operation as required and returns an object with the message and result of the operation.

## Testing Technologies

   I used both Cypress and Jest for testing the Frontend. 
   For Backend, I used Mockito and Junit.

## Authors and acknowledgment
Chance
